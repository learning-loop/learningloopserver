FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD /build/libs/*.jar app.jar
CMD ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]