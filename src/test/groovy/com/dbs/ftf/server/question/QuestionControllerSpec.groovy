package com.dbs.ftf.server.question

import com.dbs.ftf.server.model.Question
import com.dbs.ftf.server.model.Vote
import org.springframework.context.ApplicationEventPublisher
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import spock.lang.Specification

class QuestionControllerSpec extends Specification {
    def "it should create a question"(){
        def ApplicationEventPublisher eventPublisher = new MyApplicationEventPublisher()

        def QuestionService service = new QuestionService(eventPublisher)

        given: "a text needs to be created"
        def QuestionController controller = new QuestionController(service)

        when: "it is submitted to be created"
        def topic  = "Is Something Else"
        def question = controller.create(new Question(topic))

        then: "text should be added"
        question.getText() == "Is Something Else"
        question.getId().isEmpty() == false
    }

    def "it should register a vote to a specific question"(){
        def ApplicationEventPublisher eventPublisher = new MyApplicationEventPublisher()

        def QuestionService service = new QuestionService(eventPublisher)

        def QuestionController controller = new QuestionController(service)
        def topic  = "Is Something Else"

        given: "a text had been created"
        def question = controller.create(new Question(topic))

        when: "a vote is made on the survey"
        def level = 4
        def userIP = "12.1234.23.5656"
        def vote = new Vote(level)
        question = controller.vote(question.getId(), vote)

        then: "the result is captured on the text"
        question.getVoteCount() == 1
        question.getVoteLevel() == 4
    }

    def "it should list all current questions"(){
        def ApplicationEventPublisher eventPublisher = new MyApplicationEventPublisher()

        def QuestionService service = new QuestionService(eventPublisher)

        def topic  = "Is Something Else"

        given: "we need to see current questions"
        def QuestionController controller = new QuestionController(service)

        when: "a request is made to see the current questions"
        def questions = controller.get()

        then: "the current questions will be returned"
        questions.size() == 0

        when: "a new text is added"
        def question = controller.create(new Question(topic))
        questions = controller.get()


        then: "an updated list will be returned"
        questions.size() == 1


    }

    def "it should send voting results when votes are made"(){
        def eventPublisher = Mock(ApplicationEventPublisher)


        def QuestionService service = new QuestionService(eventPublisher)

        def topic  = "Is Something Else"

        given: "a text has been created"
        def QuestionController controller = new QuestionController(service)
        def question = controller.create(new Question(topic))

        when: "a stream of the results is requested"
        def SseEmitter questionStream = controller.streamQuestion(question.getId())

        then: "a stream is connected"
        questionStream instanceof SseEmitter == true

        when: "a vote is cast"
        def level = 4
        def vote = new Vote( level)
        question = controller.vote(question.getId(), vote)

        then: "an event is sent"

        1 * eventPublisher.publishEvent(question)

    }
}
