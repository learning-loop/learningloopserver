package com.dbs.ftf.server.question

import com.dbs.ftf.server.model.Question
import com.dbs.ftf.server.model.Vote
import org.springframework.context.ApplicationEventPublisher
import spock.lang.Specification

class QuestionServiceSpec extends Specification {
    def "it should create a question"(){
        def ApplicationEventPublisher eventPublisher = new MyApplicationEventPublisher()
        given: "a text needs to be created"
        def QuestionService service = new QuestionService(eventPublisher)

        when: "it is submitted to be created"
        def topic  = "Is Something Else"
        def question = service.create(new Question(topic))

        then: "text should be added"
        question.getText() == "Is Something Else"
        question.getId().isEmpty() == false
    }

    def "it should register a vote to a specific question"(){
        def ApplicationEventPublisher eventPublisher = new MyApplicationEventPublisher()

        def QuestionService service = new QuestionService(eventPublisher)
        def topic  = "Is Something Else"

        given: "a text had been created"
        def question = service.create(new Question(topic))

        when: "a vote is made on the survey"
        def level = 4
        def vote = new Vote( level)
        question = service.vote(question.getId(), vote)

        then: "the result is captured on the text"
        question.getVoteCount() == 1
        question.getVoteLevel() == 4
    }

    def "it should list all current questions"(){
        def ApplicationEventPublisher eventPublisher = new MyApplicationEventPublisher()

        def topic  = "Is Something Else"

        given: "we need to see current questions"
        def QuestionService service = new QuestionService(eventPublisher)

        when: "a request is made to see the current questions"
        def questions = service.get()

        then: "the current questions will be returned"
        questions.size() == 0

        when: "a new text is added"
        def question = service.create(new Question(topic))
        questions = service.get()


        then: "an updated list will be returned"
        questions.size() == 1

    }


}
