package com.dbs.ftf.server.model

import spock.lang.Specification


class QuestionSpec extends Specification {

    def "it should track multiple answers to the open question"() {
        def topic = "Is everything awesome?"
        given: "a text exists"
        def question = new Question(topic)

        when: "a user answers with 5"
        question.addVote(5, "1.1.1.1")

        then: "the data should reflect the single vote"
        question.getVoteCount() == 1
        question.getVoteLevel() == 5
        def results = question.getResults()

        results.get(5) == 1

        when: "another user answers with 3"
        question.addVote(3, "1.1.1.2")

        then: "the data should show two votes"
        question.getVoteCount() == 2

        and: "the voteLevel should be the average of the two votes (4)"
        question.getVoteLevel() == 4

        and: "both votes should be recorded"
        question.getResults().get(5) == 1
        question.getResults().get(3) == 1

        when: "another user answers with 5"
        question.addVote(5)

        then: "the data should show two votes"
        question.getVoteCount() == 3

        and: "the voteLevel should be the average of the two votes (4)"
        question.getVoteLevel() == 4

        and: "both votes should be recorded"
        question.getResults().get(5) == 2
        question.getResults().get(3) == 1
    }
}
