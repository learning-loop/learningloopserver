package com.dbs.ftf.server.question;

import com.dbs.ftf.server.model.Question;
import com.dbs.ftf.server.model.Vote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/v1")
public class QuestionController {
    private QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService){
        this.questionService = questionService;
    }
    @GetMapping("/questions")
    public List<Question> get(){
        return questionService.get();
    }

    @PostMapping("/questions")
    public Question create(@RequestBody Question question) {
        return questionService.create(question);
    }

    @PostMapping("/questions/{id}/vote")
    public Question vote(@PathVariable("id") String id, @RequestBody Vote vote){
        return questionService.vote(id, vote);
    }
    @PostMapping("/questions/{id}/userVote")
    public int userPreviousVote(@PathVariable("id") String id, @RequestBody Vote vote){
        return questionService.userPreviousVote(id, vote.getUserIP());
    }
    @GetMapping("/questions/{id}/allUsers")
    public List<String> userPreviousVote(@PathVariable("id") String id){
        return questionService.getAllVoters(id);
    }

    @GetMapping("/questions/{id}/stream")
    public SseEmitter streamQuestion(@PathVariable String id) throws IOException {

        return questionService.streamQuestion(id);
    }



}
