package com.dbs.ftf.server.question;

import com.dbs.ftf.server.model.Question;
import com.dbs.ftf.server.model.Vote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
@Configuration
@ComponentScan("com.dbs.ftf.server.question")
public class QuestionService {
    private Map<String, Question> questions = new HashMap<>();
    private final Map<String, CopyOnWriteArrayList<SseEmitter>> questionEmitters = new HashMap<>();


    @Autowired
    public final ApplicationEventPublisher eventPublisher;

    public QuestionService(ApplicationEventPublisher eventPublisher){
        this.eventPublisher = eventPublisher;
    }

    public List<Question> get(){
        return new ArrayList<>(questions.values());
    }

    public Question create(Question question) {
        questions.put(question.getId(), question);
        return question;
    }

    public Question vote(String id, Vote vote){
        Question question = questions.get(id);

        question.addVote(vote.getLevel(), vote.getUserIP());
        this.eventPublisher.publishEvent(question);

        return question;
    }

    public int userPreviousVote(String id, String userIP){
        Question question = questions.get(id);
        return question.hasUserAlreadyVoted(userIP);
    }
    public List<String> getAllVoters(String id){
        Question question = questions.get(id);
        if (question.getAllVoters().isEmpty()) {
            return new ArrayList<>();
        }
        return new ArrayList(question.getAllVoters().keySet());
    }
    public SseEmitter streamQuestion(String id) throws IOException {
        SseEmitter emitter = new SseEmitter(600_000L);

        if(this.questionEmitters.isEmpty()){
            this.questionEmitters.put(id, new CopyOnWriteArrayList<SseEmitter>());
        }

        if(!this.questionEmitters.containsKey(id)){
            this.questionEmitters.put(id, new CopyOnWriteArrayList<SseEmitter>());
        }

        this.questionEmitters.get(id).add(emitter);

        emitter.onCompletion(()-> this.questionEmitters.get(id).remove(emitter));
        emitter.onTimeout(() -> this.questionEmitters.get(id).remove(emitter));
        emitter.send(questions.get(id));
        return emitter;
    }

    @EventListener
    public void onVote(Question question){
        if(!this.questionEmitters.isEmpty()){
            List<SseEmitter> deadEmitters = new ArrayList<>();
            this.questionEmitters.get(question.getId()).forEach(emitter -> {
                try {
                    emitter.send(question);
                }
                catch (Exception e) {
                    deadEmitters.add(emitter);
                }
            });

            this.questionEmitters.get(question.getId()).removeAll(deadEmitters);
        }
    }
}
