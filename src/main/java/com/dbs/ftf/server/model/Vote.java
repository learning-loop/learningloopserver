package com.dbs.ftf.server.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Vote {
	
	private int level;
	private String userIP;

	@JsonCreator
	public Vote(@JsonProperty("level") int level, String user) {
		this.level = level;
		this.userIP = user;

	}
	public Vote(@JsonProperty("level") int level) {
		this.level = level;
		this.userIP = "0.0.0.0";

	}

	public int getLevel() {
		return level;
	}
	public String getUserIP() {
		return userIP;
	}
	
	
	public void setLevel(int level) {
		this.level = level;
	}
	public void setUserIP(String user) {this.userIP = user;}
	
	public boolean equals(Object other) {
		if (other instanceof Vote ) {
			return ((Vote)other).getLevel() == this.level;
		}
		
		return false;
	}

	public boolean isSameUser(String ipAddress) {
		return ipAddress == this.userIP;
	}

	public int hashCode() {
		return Objects.hash(level);
	}
}
