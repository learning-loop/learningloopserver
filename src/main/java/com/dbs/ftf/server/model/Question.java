package com.dbs.ftf.server.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Question {
    private static final int MIN_VOTE_LEVEL = 0;
    private static final int MAX_VOTE_LEVEL = 5;

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private String text;
    private String id;

    private static final Random SharedRand = new Random();

    private int voteLevel = 0;
    private int voteTotal = 0;
    private int voteCount = 0;

    private Map<Integer, Integer> results= new HashMap<>();
    private Map<String, Integer> voters = new HashMap<>();

    @JsonCreator
    public Question(@JsonProperty String text){
        this.text = text;
        this.id = randomAlphaNumeric(8);
        createResults();
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return this.text;
    }


    public void addVote(int level, String userIP) {
        addVoteHelper(level, userIP);
    }
    public void addVote(int level) {
        addVoteHelper(level, null);
    }
    public void addVoteHelper(int level, String userIP) {

        if (level > MAX_VOTE_LEVEL) {
            throw new IllegalArgumentException(String.format("Error: Submitted vote must be at least %d.", MAX_VOTE_LEVEL));
        }

        if( level < MIN_VOTE_LEVEL) {
            throw new IllegalArgumentException(String.format("Error: Submitted vote must be at least %d.", MIN_VOTE_LEVEL));
        }

        updateResults(level);
        this.voteTotal += level;
        this.voteCount++;
        this.voteLevel = voteTotal / voteCount;
        this.addNewVoter(userIP, level);
    }

    private void createResults(){
        this.results.put(0, 0);
        this.results.put(1, 0);
        this.results.put(2, 0);
        this.results.put(3, 0);
        this.results.put(4, 0);
        this.results.put(5, 0);
    }

    private void updateResults(int level){
        this.results.put(level, this.results.get(level)+1);
    }
    private void addNewVoter(String user, int level) {
        if (user != null) {
            this.voters.put(user, level);
        }
    }

    public Map<Integer, Integer> getResults() {
        return results;
    }
    public Map<String, Integer> getAllVoters() {
        return voters;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public int getVoteLevel() {
        return voteLevel;
    }

    private String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = SharedRand.nextInt(ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }
    public int hasUserAlreadyVoted(String userIP) {

        if(this.voters.get(userIP) != null) {
            return this.voters.get(userIP);
        }
        return -1;
    }
}
