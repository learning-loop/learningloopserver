# LearningLoopServer

Server-side app for FistToFive web app.

The build.gradle file includes plugins for Intellij and Eclipse so if you run “gradle idea” or “gradle eclipse” it should create the project files for your IDE.

---
Don't forget to allow the kupernetes pod to correctly check deployment health by updating the pod's YAML to reflect:

    livenessProbe:
      httpGet:
    	path: /actuator
    
    readinessProbe:
      httpGet:
    	path: /actuator

If you do not, the pod will enter a crash loop state.

---
The API is a simplified REST style

GET

    /v1/questions — return the current questions
    /v1/questions/{id}/allUsers - returns the current users
    /v1/questions/{id}/stream — streams updates from a question

POST

    /v1/questions - create a new question
    /v1/questions/{id}/vote - cast a vote on a question
    /v1/questions/{id}/userVote - used to check if user has already voted on a question